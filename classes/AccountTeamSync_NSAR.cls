/******************************************************************************
 * Name: AccountTeamSync_NSAR
 * Description: Class that reflects changes from the Account team object to the 
 *              to specified fields on the Non Standard Agreement Request.
 * Change Log
 * Item #   Date        Author          Description
 *    1     1/17/2014   Ben Logan        Created
 ******************************************************************************/
global class AccountTeamSync_NSAR implements Database.Batchable<sObject>{
    private List<AccountTeamMember> ATeam_Members = new List<AccountTeamMember>();
    private Set<ID> FacilityIDs = new Set<ID>();
    private Set<Non_Standard_Agreement_Request__c> SetNSAR = new Set<Non_Standard_Agreement_Request__c>();
    private Map<ID, set<Non_Standard_Agreement_Request__c>> MapNSAR = new Map<ID, set<Non_Standard_Agreement_Request__c>>();
    private list<Non_Standard_Agreement_Request__c> Final_lNSAR=new list<Non_Standard_Agreement_Request__c>();
    private list<Non_Standard_Agreement_Request__c> lNSAR = new list<Non_Standard_Agreement_Request__c>();

    public AccountTeamSync_NSAR(){

    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      String Query;

        Query = 'select id, Group_VP__c, Client_Service_VP__c, Section_VP__c, '+
                'Provider_Facility_Association__r.Hospital__c, '+
                'Provider_Facility_Association__r.Credentialing_Status__c '+
                'from Non_Standard_Agreement_Request__c';

        return Database.getQueryLocator(Query);
    }
   
    global void execute(Database.BatchableContext BC, list<Non_Standard_Agreement_Request__c> scope)
    {
        Map<ID, set<Non_Standard_Agreement_Request__c>> MapNSAR = new Map<ID, set<Non_Standard_Agreement_Request__c>>();
        Set<ID> FacilityIDs = new Set<ID>();
        Set<Non_Standard_Agreement_Request__c> SetNSAR = new Set<Non_Standard_Agreement_Request__c>();
        list<Non_Standard_Agreement_Request__c> Final_lNSAR=new list<Non_Standard_Agreement_Request__c>();


        for(Non_Standard_Agreement_Request__c NSAR:scope){
          ID HospitalID=NSAR.Provider_Facility_Association__r.Hospital__c;
          set<Non_Standard_Agreement_Request__c> TempSet = new set<Non_Standard_Agreement_Request__c>();
            
          if(MapNSAR.containsKey(HospitalID)){
              TempSet = MapNSAR.get(HospitalID);
              TempSet.add(NSAR);
              MapNSAR.get(HospitalID);
          }else{
              MapNSAR.put(HospitalID, new set<Non_Standard_Agreement_Request__c>{NSAR});
          }
            
          FacilityIDs.add(HospitalID);
        }

        list<AccountTeamMember> Ateam=[SELECT Id, TeamMemberRole, AccountId, LastModifiedDate, UserId 
                                       FROM AccountTeamMember WHERE AccountId IN:FacilityIDs 
                                       and (TeamMemberRole='Group VP'
                                       or TeamMemberRole='Client Service VP'
                                       or TeamMemberRole='Section VP')];

        for(set<Non_Standard_Agreement_Request__c> NSARset:MapNSAR.values()){
            for(Non_Standard_Agreement_Request__c NSAR:NSARset){
                 for(AccountTeamMember ATeam_Member:Ateam){
                    if(ATeam_Member.TeamMemberRole=='Group VP'){
                        NSAR.Group_VP__c=ATeam_Member.UserId;
                    }
                    if(ATeam_Member.TeamMemberRole=='Client Service VP'){
                        NSAR.Client_Service_VP__c=ATeam_Member.UserId;
                    }
                    if(ATeam_Member.TeamMemberRole=='Section VP'){
                        NSAR.Section_VP__c=ATeam_Member.UserId;
                    }
                 }
                SetNSAR.add(NSAR);
            }
        }
    
        Final_lNSAR=new list<Non_Standard_Agreement_Request__c>();
        Final_lNSAR.addAll(SetNSAR);
        update Final_lNSAR;
 
    }   
    
    global void finish(Database.BatchableContext BC)
    {
             
            
    }
    
}