/******************************************************************************
 * Name: AccountTeamSync_PFA
 * Description: Class that reflects changes from the Account team object to the 
 *              to specified fields on the PFA.
 * Change Log
 * Item #   Date        Author          Description
 *    1     1/07/2014   Ben Logan        Created
 ******************************************************************************/
global class AccountTeamSync_PFA implements Schedulable{
    
    global void execute(SchedulableContext sc) {     
                Id batjobId = Database.executeBatch(new AccountTeamSync_PFA_Batch(), 1);  
    }
    

}