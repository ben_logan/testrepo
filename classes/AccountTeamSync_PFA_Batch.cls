/******************************************************************************
 * Name: AccountTeamSync_PFA_Batch
 * Description: Class that reflects changes from the Account team object to the 
 *              to specified fields on the PFA. This class is dependent on 
 *              AccountTeamSync_PFA for scheduling.
 * Change Log
 * Item #   Date        Author          Description
 *    1     1/07/2014   Ben Logan        Created
 *	  2     11/03/2015  Ben Logan	     Added features requested in WR-081415-5587 and WR-081915-5597
 ******************************************************************************/
global class AccountTeamSync_PFA_Batch implements Database.Batchable<sObject>,
                                                                Database.Stateful{
   
    private List<AccountTeamMember> ATeam_Members = new List<AccountTeamMember>();
    private Integration_Log__c ilog = new Integration_Log__c();
    private String ILogString='PFA_ID,FacilityID,PFA_Field_Updated,PFA_Old_Value,PFA_New_Value,Account_Team_Role\r\n';
    private integer BatchCount=0;
    private integer Facility_Count=0;
    public string query = '';
	Map<String,List<String>> FacWithAteam = new Map<String,List<String>>();
	List<Account> BH_FacChange_Accounts = new List<Account>();
    
    global AccountTeamSync_PFA_Batch(string param){
           query='SELECT Id, TeamMemberRole, AccountId, Account.Name, Account.GlobalFacilityID__c, LastModifiedDate,'+ 
			     'UserId, User.Name, Account.Update_Flag__c '+ 
                 'FROM AccountTeamMember WHERE TeamMemberRole in(\''+'VP of Provider Services'+'\',\''+
				 'Group MO'+'\',\''+
				 'Section MO'+'\',\''+
				 'Client Service MO'+'\',\''+
				 'Client Service VP'+'\',\''+
				 'Recruiter-Primary'+'\',\''+
				 'Group VP'+'\',\''+
				 'Section VP'+'\') '+
                 'and LastModifiedDate>='+param;
    }
    
    global AccountTeamSync_PFA_Batch(){
           query='SELECT Id, TeamMemberRole, AccountId, Account.Name, Account.GlobalFacilityID__c, LastModifiedDate,'+ 
		         'UserId, User.Name, Account.Update_Flag__c '+ 
                 'FROM AccountTeamMember WHERE TeamMemberRole in(\''+'VP of Provider Services'+'\',\''+
				 'Group MO'+'\',\''+
				 'Section MO'+'\',\''+
				 'Client Service MO'+'\',\''+
				 'Client Service VP'+'\',\''+
				 'Recruiter-Primary'+'\',\''+
				 'Group VP'+'\',\''+
				 'Section VP'+'\') '+
				 'and LastModifiedDate>=YESTERDAY';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        ilog.Process_Type__c='Account Team Sync - PFA';
        insert ilog;

        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, list<AccountTeamMember> scope)
    {
        Map<ID, Map<String,List<ID>>> ATeam_With_FacilityIDs = new  Map<ID, Map<String,List<ID>>>();
        Map<ID, List<Provider_Facility_Associations__c>> PFA_Map = new Map<ID, List<Provider_Facility_Associations__c>>();
        Map<Integer, List<Provider_Facility_Associations__c>> Final_PFAs=new Map<Integer, List<Provider_Facility_Associations__c>>();
        
		List<Provider_Facility_Associations__c> PFAs_Finalized = new List<Provider_Facility_Associations__c>();
        Set<Provider_Facility_Associations__c> PFAs_Finalized_Set = new Set<Provider_Facility_Associations__c>(); 
        Set<ID> FacilityIDs = new Set<ID>();
		Set<ID> BH_FacChange_FACIDs = new Set<ID>();

        for(AccountTeamMember ATeam_Member:scope){
            list<ID> TempList = new list<ID>();
			List<String> OrigamiTempList = new List<String>();
			string OrigamiTempString;
			
            TempList.add(ATeam_Member.AccountId);

			OrigamiTempString = ATeam_Member.Account.GlobalFacilityID__c + ',' + ATeam_Member.Account.Name;
			

            ATeam_With_FacilityIDs.put(ATeam_Member.UserId,new Map<String,list<ID>>{ATeam_Member.TeamMemberRole=>TempList});
			
			if (FacWithAteam.containsKey(OrigamiTempString))
			{
				OrigamiTempList = FacWithAteam.get(OrigamiTempString);
				OrigamiTempList.add(ATeam_Member.TeamMemberRole +','+ATeam_Member.User.Name);

			}else{
				FacWithAteam.put(OrigamiTempString, new List<String>{ATeam_Member.TeamMemberRole +','+ATeam_Member.User.Name});
			}
			
			if (ATeam_Member.TeamMemberRole=='Recruiter-Primary')
			{
				BH_FacChange_FACIDs.add(ATeam_Member.AccountId);
			}


            FacilityIDs.add(ATeam_Member.AccountId);
        }

        Facility_Count=FacilityIDs.size();

        List<Provider_Facility_Associations__c> lpfa = [SELECT id, Standard_Agreement_Approver__c, Hospital__c 
                                                   FROM Provider_Facility_Associations__c 
                                                   WHERE Hospital__c IN:FacilityIDs 
                                                   AND Credentialing_Status__c!='Inactive'];

		List<Account> BH_FacChange_TempFAC = [select Id, update_flag__c from account where id in:BH_FacChange_FACIDs];

		for (Account acc:BH_FacChange_TempFAC)
		{
			if (acc.update_flag__c==false)
			{
				acc.update_flag__c=true;
			}else{
				acc.update_flag__c=false;
			}

			BH_FacChange_Accounts.add(acc);
		}
		


        for(Provider_Facility_Associations__c PFA:lpfa){
            list<Provider_Facility_Associations__c> TempList=new list<Provider_Facility_Associations__c>();
            if(PFA_Map.containsKey(PFA.Hospital__c)){
                TempList = PFA_Map.get(PFA.Hospital__c);
                TempList.add(PFA);
                PFA_Map.get(PFA.Hospital__c);
            }else{
                PFA_Map.put(PFA.Hospital__c, new List<Provider_Facility_Associations__c>{PFA});
            }
        }

        for(Map<String,List<ID>> AteamMap:ATeam_With_FacilityIDs.values()){
            for(List<ID> TMR:AteamMap.values()){
                if(TMR!=null){
                    for(ID FACID:TMR){
                        if(PFA_Map.get(FACID)!=null){
                            for(Provider_Facility_Associations__c PFA:PFA_Map.get(FACID)){
                                for(ID key:ATeam_With_FacilityIDs.keyset()){
                                    if(AteamMap==ATeam_With_FacilityIDs.get(key)){
                                        if(AteamMap.containsKey('VP of Provider Services')==true){
                                            ILogString=ILogString+
                                                       PFA.Id+','+
                                                       PFA.Hospital__c+','+
                                                       'Standard_Agreement_Approver__c,'+
                                                       PFA.Standard_Agreement_Approver__c+','+
                                                       key+','+
                                                       'VP of Provider Services\r\n';
                                            PFA.Standard_Agreement_Approver__c=key;

                                            PFAs_Finalized_Set.add(PFA);
                                        }           
                                    }
                                }
                            }
                        }   
                    } 
                }
            }
        }     
        
        PFAs_Finalized.addall(PFAs_Finalized_Set);
        try{
            Database.SaveResult[] saveResults = Database.update(PFAs_Finalized);
            
            for (Database.SaveResult saveResult : saveResults) {
                if (saveResult.isSuccess()) {
                    ilog.Status__c = 'Completed';
                }else{
                    ilog.Status__c = 'Completed With Errors';
                    ilog.Comments__c = ilog.Comments__c + saveResult.getId() + '.\r\n' + saveResult.getErrors() + '.\r\n';
                }
            }
        }catch (DMLException e){
            ilog.Status__c = 'DML Exception - Did Not Complete.';
        }

        update ilog;
		
		
    }   
    
    global void finish(Database.BatchableContext BC)
    {

        //ilog.Comments__c=String.valueOf(FacilityIDs);
        //update ilog;
                         
        Attachment ilogAttachment=new Attachment(body=blob.valueOf(ILogString), Name='Transaction Data.csv', 
                                                     description='Account Team Sync - PFA Transaction Data\r\n Facilities Updated:'+Facility_Count,
                                                     parentId=ilog.Id);
        insert ilogAttachment;
		                
        OrigamiSuppMailer(FacWithAteam);

		update BH_FacChange_Accounts;    
    }

    //Method called in PFAAfter trigger so changes are reflected on updated PFA instantaniously. 
    public void AccountTeamSync_PFA(list<Provider_Facility_Associations__c> lPFA, set<ID> AccID){
        
        System.Debug('####### '+ lPFA);
        System.Debug('####### '+ AccID);
        Map<ID, Map<String,List<ID>>> ATeam_With_FacilityIDs = new  Map<ID, Map<String,List<ID>>>();
        Map<ID, List<Provider_Facility_Associations__c>> PFA_Map = new Map<ID, List<Provider_Facility_Associations__c>>();
        Map<Integer, List<Provider_Facility_Associations__c>> Final_PFAs=new Map<Integer, List<Provider_Facility_Associations__c>>();
        List<Provider_Facility_Associations__c> PFAs_Finalized = new List<Provider_Facility_Associations__c>();
        Set<Provider_Facility_Associations__c> PFAs_Finalized_Set = new Set<Provider_Facility_Associations__c>(); 
        Set<ID> FacilityIDs = new Set<ID>();

            list<AccountTeamMember> lTMR = [SELECT Id, TeamMemberRole, AccountId, LastModifiedDate, UserId 
                                           FROM AccountTeamMember WHERE AccountId in: AccID and 
                                           TeamMemberRole in('VP of Provider Services')];

            for(AccountTeamMember ATeam_Member:lTMR){
                list<ID> TempList=new list<ID>();

                TempList.add(ATeam_Member.AccountId);

                ATeam_With_FacilityIDs.put(ATeam_Member.UserId,new Map<String,list<ID>>{ATeam_Member.TeamMemberRole=>TempList});

                FacilityIDs.add(ATeam_Member.AccountId);
            }

            for(Provider_Facility_Associations__c PFA:lPFA){
                    list<Provider_Facility_Associations__c> TempList=new list<Provider_Facility_Associations__c>();
                    if(PFA_Map.containsKey(PFA.Hospital__c)){
                        TempList = PFA_Map.get(PFA.Hospital__c);
                        TempList.add(PFA);
                        PFA_Map.get(PFA.Hospital__c);
                    }else{
                        PFA_Map.put(PFA.Hospital__c, new List<Provider_Facility_Associations__c>{PFA});
                    }
            }

            for(Map<String,List<ID>> AteamMap:ATeam_With_FacilityIDs.values()){
                for(List<ID> TMR:AteamMap.values()){
                    if(TMR!=null){
                        for(ID FACID:TMR){
                            if(PFA_Map.get(FACID)!=null){
                                for(Provider_Facility_Associations__c PFA:PFA_Map.get(FACID)){
                                    for(ID key:ATeam_With_FacilityIDs.keyset()){
                                        if(AteamMap==ATeam_With_FacilityIDs.get(key)){
                                            if(AteamMap.containsKey('VP of Provider Services')==true){

                                                PFA.Standard_Agreement_Approver__c=key;

                                                PFAs_Finalized_Set.add(PFA);
                                            }           
                                        }
                                    }
                                }
                            }   
                        } 
                    }
                }
            } 

            PFAs_Finalized.addall(PFAs_Finalized_Set);
            update PFAs_Finalized;
    }
	
	public void OrigamiSuppMailer(Map<String,List<String>> FacAndATeams){

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	    List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		List<String> sendTo = new List<String>();
		sendTo.add('ben_logan@schumachergroup.com');
		mail.setToAddresses(sendTo);
		mail.setReplyTo('noreply@schumachergroup.com');
        mail.setSenderDisplayName('NoReply');
		mail.setSubject('Account Team Members That Have Changed');

		String body='';
		ApexUtilities.HTMLTable InsertTable = new ApexUtilities.HTMLTable();
        InsertTable.setStandardBlueBorder();
        List<String> InsertHeaders = new List<String>();
		InsertHeaders.add('Global Facility ID');
        InsertHeaders.add('Facility');
        InsertHeaders.add('Account Team Member');
        InsertHeaders.add('Account Team Member Role');
		InsertTable.setHeaderOrder(InsertHeaders);

		for(String FACs:FacAndATeams.keySet())
		{
			for (String Ateam:FacAndATeams.get(FACs))
			{
					String[] FACGlobSplit = FACs.split(',');
					String FACi = FACGlobSplit[0];
					String GlobFac = FACGlobSplit[1];

					String[] AteamSplit = Ateam.split(',');
					String Member = AteamSplit[0];
					String Team = AteamSplit[1];

					InsertTable.NewRow();
				
					InsertTable.AddRowStyle('background:rgb(255, 240, 240);Color:Blue;');

					InsertTable.AddElement('Global Facility ID', FACi);
					InsertTable.AddElement('Facility', GlobFac);
					InsertTable.AddElement('Account Team Member', Team);
					InsertTable.AddElement('Account Team Member Role', Member);			
			}
		}
		Map<String, String> ReturnMap = new Map<String, String>();
       
	    body=body + InsertTable.ConstructHTMLTable('Account Team Members That Have Changed');

		mail.sethtmlbody(body);
		mails.add(mail);
		Messaging.sendEmail(mails);
	}
}