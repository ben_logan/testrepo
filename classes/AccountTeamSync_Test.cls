/******************************************************************************
 * Name: AccountTeamSync_Test
 * Description: This class test AccountTeamReport.cls and AccountTeamSync_PFA_Batch.cls
 * Change Log
 * Item #   Date        Author          Description
 *    1     11/24/2015   Ben Logan       Created
 ******************************************************************************/
@isTest 
private class AccountTeamSync_Test {

    static testmethod void Scenerio01(){
        Account acc1 = TestUtilities.createAccount(new Map<String, Object>{
                                            'IT_ADT_Email__c'=> 'Chris_Hoback@schumachergroup.com',
											'update_flag__c'=> True
                                        });

        Facility_Profile__c facPro1 = TestUtilities.createFacilityProfile(new Map<String, Object>{
                                            'Facility_Name__c' => acc1.Id
                                        }, TRUE);

        Contact con1 = TestUtilities.createContact(null, acc1, TRUE);

        Provider_Facility_Associations__c PFA1 = TestUtilities.createProviderFacilityAssociation(
                            new Map<String, Object>{
                                'Status__c' => 'active',
                                'Credentialing_Status__c' => 'active',
                                'Billing_Company__c' => 'DST',
                                'Facility_Title__c' => 'Assistant Medical Director'

                            }, //Map<String, Object> fieldNameValueMapping, 
                            acc1, //Account acc, 
                            con1, //Contact prov, 
                            null, //Billing__c bc);
                            TRUE);
        
		Account acc2 = TestUtilities.createAccount(new Map<String, Object>{
                                            'IT_ADT_Email__c'=> 'Chris_Hoback@schumachergroup.com',
											'update_flag__c'=> False
                                        });

        Facility_Profile__c facPro2 = TestUtilities.createFacilityProfile(new Map<String, Object>{
                                            'Facility_Name__c' => acc2.Id
                                        }, TRUE);

        Contact con2 = TestUtilities.createContact(null, acc2, TRUE);

        Provider_Facility_Associations__c PFA2 = TestUtilities.createProviderFacilityAssociation(
                            new Map<String, Object>{
                                'Status__c' => 'active',
                                'Credentialing_Status__c' => 'active',
                                'Billing_Company__c' => 'DST',
                                'Facility_Title__c' => 'Assistant Medical Director'

                            }, //Map<String, Object> fieldNameValueMapping, 
                            acc2, //Account acc, 
                            con2, //Contact prov, 
                            null, //Billing__c bc);
                            TRUE);

		List<Provider_Facility_Associations__c> pfalist = new List<Provider_Facility_Associations__c>();
		pfalist.add(PFA1);
		pfalist.add(PFA2);

		Set<Id> AccSet = new Set<Id>();
		AccSet.add(acc1.id);
		AccSet.add(acc2.id);

		User ateammember = TestUtilities.createUser(true);
		User ateammember2 = TestUtilities.createUser(true);

         List<AccountTeamMember> Ateams = new List<AccountTeamMember>();

		 AccountTeamMember VPofProviderServices = new AccountTeamMember();
		 VPofProviderServices.AccountID = acc1.id;
		 VPofProviderServices.UserID = ateammember.id;
		 VPofProviderServices.TeamMemberRole = 'VP of Provider Services';
		 
		 Ateams.add(VPofProviderServices);

		 AccountTeamMember GroupMO = new AccountTeamMember();
		 GroupMO.AccountID = acc1.id;
		 GroupMO.UserID = ateammember.id;
		 GroupMO.TeamMemberRole = 'Group MO';

		 Ateams.add(GroupMO);

		 AccountTeamMember SectionMO = new AccountTeamMember();
		 SectionMO.AccountID = acc1.id;
		 SectionMO.UserID = ateammember.id;
		 SectionMO.TeamMemberRole = 'Section MO';

		 Ateams.add(SectionMO);

		 AccountTeamMember ClientServiceMO = new AccountTeamMember();
		 ClientServiceMO.AccountID = acc1.id;
		 ClientServiceMO.UserID = ateammember.id;
		 ClientServiceMO.TeamMemberRole = 'Client Service MO';

		 Ateams.add(ClientServiceMO);

		 AccountTeamMember ClientServiceVP = new AccountTeamMember();
		 ClientServiceVP.AccountID = acc1.id;
		 ClientServiceVP.UserID = ateammember.id;
		 ClientServiceVP.TeamMemberRole = 'Client Service VP';

		 Ateams.add(ClientServiceVP);

		 AccountTeamMember RecruiterPrimary = new AccountTeamMember();
		 RecruiterPrimary.AccountID = acc1.id;
		 RecruiterPrimary.UserID = ateammember.id;
		 RecruiterPrimary.TeamMemberRole = 'Recruiter-Primary';

		 Ateams.add(RecruiterPrimary);

		 AccountTeamMember GroupVP = new AccountTeamMember();
		 GroupVP.AccountID = acc1.id;
		 GroupVP.UserID = ateammember.id;
		 GroupVP.TeamMemberRole = 'Group VP';

		 Ateams.add(GroupVP);

		 AccountTeamMember SectionVP = new AccountTeamMember();
		 SectionVP.AccountID = acc1.id;
		 SectionVP.UserID = ateammember.id;
		 SectionVP.TeamMemberRole = 'Section VP';

		 Ateams.add(SectionVP);

		 AccountTeamMember VPofProviderServices2 = new AccountTeamMember();
		 VPofProviderServices2.AccountID = acc2.id;
		 VPofProviderServices2.UserID = ateammember2.id;
		 VPofProviderServices2.TeamMemberRole = 'VP of Provider Services';
		 
		 Ateams.add(VPofProviderServices2);

		 AccountTeamMember GroupMO2 = new AccountTeamMember();
		 GroupMO2.AccountID = acc2.id;
		 GroupMO2.UserID = ateammember2.id;
		 GroupMO2.TeamMemberRole = 'Group MO';

		 Ateams.add(GroupMO2);

		 AccountTeamMember SectionMO2 = new AccountTeamMember();
		 SectionMO2.AccountID = acc2.id;
		 SectionMO2.UserID = ateammember2.id;
		 SectionMO2.TeamMemberRole = 'Section MO';

		 Ateams.add(SectionMO2);

		 AccountTeamMember ClientServiceMO2 = new AccountTeamMember();
		 ClientServiceMO2.AccountID = acc2.id;
		 ClientServiceMO2.UserID = ateammember2.id;
		 ClientServiceMO2.TeamMemberRole = 'Client Service MO';

		 Ateams.add(ClientServiceMO2);

		 AccountTeamMember ClientServiceVP2_1 = new AccountTeamMember();
		 ClientServiceVP2_1.AccountID = acc1.id;
		 ClientServiceVP2_1.UserID = ateammember2.id;
		 ClientServiceVP2_1.TeamMemberRole = 'Client Service VP';

		 Ateams.add(ClientServiceVP2_1);

		 AccountTeamMember ClientServiceVP1_2 = new AccountTeamMember();
		 ClientServiceVP1_2.AccountID = acc2.id;
		 ClientServiceVP1_2.UserID = ateammember.id;
		 ClientServiceVP1_2.TeamMemberRole = 'Client Service VP';

		 Ateams.add(ClientServiceVP1_2);

		 AccountTeamMember ClientServiceVP2 = new AccountTeamMember();
		 ClientServiceVP2.AccountID = acc2.id;
		 ClientServiceVP2.UserID = ateammember2.id;
		 ClientServiceVP2.TeamMemberRole = 'Client Service VP';

		 Ateams.add(ClientServiceVP2);

		 AccountTeamMember RecruiterPrimary2 = new AccountTeamMember();
		 RecruiterPrimary2.AccountID = acc2.id;
		 RecruiterPrimary2.UserID = ateammember2.id;
		 RecruiterPrimary2.TeamMemberRole = 'Recruiter-Primary';

		 Ateams.add(RecruiterPrimary2);

		 AccountTeamMember GroupVP2 = new AccountTeamMember();
		 GroupVP2.AccountID = acc2.id;
		 GroupVP2.UserID = ateammember2.id;
		 GroupVP2.TeamMemberRole = 'Group VP';

		 Ateams.add(GroupVP2);

		 AccountTeamMember SectionVP2 = new AccountTeamMember();
		 SectionVP2.AccountID = acc2.id;
		 SectionVP2.UserID = ateammember2.id;
		 SectionVP2.TeamMemberRole = 'Section VP';

		 Ateams.add(SectionVP2);

		 insert Ateams;
		 
			AccountTeamSync_PFA_Batch ATeamSyncBatch = new AccountTeamSync_PFA_Batch();

			AccountTeamReport ateamreport = new AccountTeamReport();
			String ateamreportTestString  = 'true&true&true&A,B&1,2,3,4,Care Management,No Region Entered&AK,AL,AR,AZ,CA,CO,CT,DC,DE,FL,GA,HI,IA,ID,IL,IN,KS,KY,LA,MA,MD,ME,MI,MN,MO,MS,MT,NA,NC,ND,NE,NH,NI,NJ,NM,NV,NY,OH,OK,ON,OR,PA,PE,PR,QC,RI,SC,SD,TN,TO,TX,UT,VA,VI,VT,WA,WI,WV,WY&S1T1,S1T2,S1T3,S2T1,S2T2,S2T3,S2T4,S2T5,S3T1,S3T2,S3T3,S3T4,S4T1,S4T2,S4T3,CM,SW I,Delta,GL I,GL II,HM Central,HM East,HM West,MW,MW II,PT,SE,SE II,SE III,SE IV,SE V,SE VI,SE VII,SW II,SW III,SW IV,SW V,UC,SC I,SC II,1,2,3,4,5,6,7,8,9';
			String ateamreportTestString2 = 'false&false&false&A,B&1,2,3,4,Care Management,No Region Entered&AK,AL,AR,AZ,CA,CO,CT,DC,DE,FL,GA,HI,IA,ID,IL,IN,KS,KY,LA,MA,MD,ME,MI,MN,MO,MS,MT,NA,NC,ND,NE,NH,NI,NJ,NM,NV,NY,OH,OK,ON,OR,PA,PE,PR,QC,RI,SC,SD,TN,TO,TX,UT,VA,VI,VT,WA,WI,WV,WY&S1T1,S1T2,S1T3,S2T1,S2T2,S2T3,S2T4,S2T5,S3T1,S3T2,S3T3,S3T4,S4T1,S4T2,S4T3,CM,SW I,Delta,GL I,GL II,HM Central,HM East,HM West,MW,MW II,PT,SE,SE II,SE III,SE IV,SE V,SE VI,SE VII,SW II,SW III,SW IV,SW V,UC,SC I,SC II,1,2,3,4,5,6,7,8,9';
		
		Test.StartTest();
			
			ATeamSyncBatch.AccountTeamSync_PFA(pfalist,AccSet);

			Id batjobId = Database.executeBatch(new AccountTeamSync_PFA_Batch(), 200);
			Id batjobId2 = Database.executeBatch(new AccountTeamSync_PFA_Batch('TODAY'), 200);
			
			List<SelectOption> cst = ateamreport.GetCST();
			List<SelectOption> sta = ateamreport.GetStates();
			List<SelectOption> rol = ateamreport.GetRoles();
			
			AccountTeamReport.getResponse(ateamreportTestString);
			AccountTeamReport.getResponse(ateamreportTestString2);

        Test.StopTest();

    }

}